# Odoo Dev Utilities
This repository contains a guide on best practices for Odoo development at SomIT, as well as a set of shell aliases to manage local LXC containers more easily for the developer. 

## Contents
- [Odoo Development Best Practices](#odoo-development-best-practices)
- [LXC Aliases](#lxc-aliases)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Odoo Development Best Practices
The guide, named `SOMIT-git-good-practices.md`, provides a step-by-step explanation of how to use Git and Gitlab in the SomIT workflow, focusing on branching, WIP (Work In Progress) commits, amending previous commits, stashing changes and pre-commit hooks.

It is recommended to read this guide before starting any development work with Odoo at SomIT.

## LXC Aliases
The script `lxc_aliases.sh` installs a set of shell aliases for managing local LXC containers using the `fzf` tool. These aliases include:
- `list`: Lists all available LXC containers.
- `logs`: Retrieves and displays the last 100 lines of the Odoo log file for a selected container.
- `start`: Starts a selected container.
- `stop`: Stops a selected container.
- `restart`: Restarts a selected container and shows realtime logs also adding the last 100 lines for context.

## Installation
To install the shell aliases, first run this commmand to install fzf:
```
sudo apt install fzf
```

And now install the aliases like this:

```
sh lxc_aliases.sh
```

This script will detect your current shell and append the aliases to the appropriate configuration file (e.g., `.bash_aliases`, `.zshrc`, or `config.fish`). It will also set write and execute permissions for the user on the configuration file.

## Usage
After installation, you can use the aliases in your terminal by typing their names followed by any necessary arguments (e.g., `start`, `stop`, `list`, `restart`, `logs` or `logs <container_name>`).

## Contributing
Feel free to contribute to this repository by submitting a merge request or opening an issue with your suggestions or bug reports.
