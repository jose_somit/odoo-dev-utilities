#!/bin/sh

# Detect user's current shell
SHELL_CONFIG=~/.bash_aliases
if [ "$SHELL" = "/bin/zsh" ]; then
    SHELL_CONFIG=~/.zshrc
elif [ "$SHELL" = "/bin/fish" ]; then
    SHELL_CONFIG=~/.config/fish/config.fish
else
    # If the shell is bash, add aliases to .bash_aliases
    SHELL_CONFIG=~/.bash_aliases
fi

# Create config file if it doesn't exist
if [ ! -f $SHELL_CONFIG ]; then
    touch $SHELL_CONFIG
fi

# Set write and execute permissions for the user
chmod u+wx $SHELL_CONFIG

# Check if aliases already exist in the config file
sudo grep -q $SHELL_CONFIG -e "alias start=" -e "alias stop=" -e "alias restart=" -e "alias list=" -e "alias logs=" || cat <<- 'EOF' > $SHELL_CONFIG
# LXC tools
alias logs='foo(){ sudo echo retrieving && sudo lxc-attach -n ${1:-$(sudo lxc-ls -1 --active | fzf)} -- sudo -u odoo bash -c "sudo systemctl restart odoo && tail -fn 100 /var/log/odoo/odoo.log"; unset -f foo;}; foo'
alias start='sudo echo loading && sudo lxc-start $(sudo lxc-ls -1 --stopped | fzf)'
alias stop='sudo echo loading && sudo lxc-stop $(sudo lxc-ls -1 --active | fzf)'
alias restart='sudo echo loading && CONTAINER=$(sudo lxc-ls -1 --active | fzf) && sudo lxc-stop $CONTAINER && sudo lxc-start $CONTAINER && sleep 0.5 && logs $CONTAINER'
alias list='sudo lxc-ls -f'
EOF

# Notify user that aliases were installed successfully (or updated)
echo "Aliases installed successfully!"
