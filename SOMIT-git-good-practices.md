## Git Workflow Guide
This guide will provide you with the necessary steps to follow a standardized workflow using Git and Gitlab, focusing on branching, WIP (Work In Progress) commits, amending previous commits, stashing changes, pre-commit hooks, and checking the status of your changes.

### Table of Contents
- [Git Workflow Guide](#git-workflow-guide)
  - [Table of Contents](#table-of-contents)
  - [1. Branching and Merging](#1-branching-and-merging)
    - [1.1 Creating a new branch](#11-creating-a-new-branch)
    - [1.2 Switching between branches](#12-switching-between-branches)
    - [1.3 Merging changes from one branch to another](#13-merging-changes-from-one-branch-to-another)
  - [2. Work In Progress (WIP) Commits](#2-work-in-progress-wip-commits)
    - [2.1 Creating a WIP commit](#21-creating-a-wip-commit)
    - [2.2 Amending a previous commit](#22-amending-a-previous-commit)
      - [2.2.1 Modifying the last commit message](#221-modifying-the-last-commit-message)
      - [2.2.2 Adding changes to the last commit](#222-adding-changes-to-the-last-commit)
      - [2.2.3 Checking the status of your changes](#223-checking-the-status-of-your-changes)
      - [2.2.4 Restoring staged files](#224-restoring-staged-files)
  - [3. Stashing changes](#3-stashing-changes)
    - [3.1 Saving uncommitted changes with stash](#31-saving-uncommitted-changes-with-stash)
      - [3.1.1 Creating a new stash](#311-creating-a-new-stash)
      - [3.1.2 Listing all stashes](#312-listing-all-stashes)
    - [3.2 Applying previously stashed changes](#32-applying-previously-stashed-changes)
      - [3.2.1 Applying a specific stash](#321-applying-a-specific-stash)
      - [3.2.2 Deleting a specific stash](#322-deleting-a-specific-stash)
    - [3.3 Popping a stash](#33-popping-a-stash)
  - [4. Pre-commit Hooks](#4-pre-commit-hooks)
  - [5. Conclusion](#5-conclusion)

## 1. Branching and Merging
### 1.1 Creating a new branch
To create a new branch, run:
```
git checkout -b <branch_name>
```
This will create a new branch with the specified name and switch to it immediately.

### 1.2 Switching between branches
To switch between branches, use:
```
git checkout <branch_name>
```
Replace `<branch_name>` with the name of the branch you want to switch to.

### 1.3 Merging changes from one branch to another
1. First, make sure you're on the branch where you want to merge the changes (e.g. `main`).
```
git checkout main
```
2. Then, merge the changes from the other branch:
```
git merge <branch_name>
```
Replace `<branch_name>` with the name of the branch containing the changes you want to merge.

> Although you might not need this operation because within the Gitlab workflow you can create **Merge Requests** (MR) from the pushed branch that will automatically merge to main branch once closed.

## 2. Work In Progress (**WIP**) Commits
### 2.1 Creating a **WIP** commit
To create a **WIP** commit, simply add and commit your changes as usual:
```
git add .
git commit -m "WIP"
```
This will create a new commit with the message "WIP". You can push this commit to Gitlab without any issues.

### 2.2 **Amending** a previous commit
An **amend** in Git is the process of modifying an existing commit. You can use this to change a commit message or add changes that were previously unstaged. Amending is useful when you realize you forgot to include something in your last commit, or if you want to combine multiple commits into one. To **amend** a commit, you use the --amend flag with the git commit command. Be careful when amending commits that have already been pushed to a shared repository, as this can cause conflicts for other collaborators.
#### 2.2.1 Modifying the last commit message
To modify the last commit's message, follow these steps:
1. Use the `--amend` flag when committing with the `-m` option:
```
git commit --amend -m "New commit message"
```
Replace "New commit message" with the updated message for your commit. This will modify the previous commit's message without changing its contents.

2. Force push to Gitlab (be careful, as this can overwrite other people's work):
```
git push --force-with-lease origin <branch_name>
```
> *--force-with-lease*[<sup>doc</sup>][force-with-lease] is a safer option of forcing that will not overwrite any work on the remote branch if more commits were added to the remote branch since you pulled.

[force-with-lease]: https://git-scm.com/docs/git-push#Documentation/git-push.txt---force-with-leaseltrefnamegt


Replace `<branch_name>` with the name of the branch you're working on.

#### 2.2.2 Adding changes to the last commit
To add changes to the last commit, follow these steps:
1. Make your changes and stage them as usual:
```
git add .
```
2. Use the `--amend` flag when committing without the `-m` option:
```
git commit --amend
```
This will open your default text editor to modify the previous commit's message (if any). Save and close the editor to continue. The new changes will be added to the last commit.
3. Force push to Gitlab (be careful, as this can overwrite other people's work):
```
git push --force-with-lease origin <branch_name>
```
Replace `<branch_name>` with the name of the branch you're working on.

#### 2.2.3 Checking the status of your changes
To check the status of your changes, use:
```
git status
```
This command will display a list of modified files, staged files, and untracked files in your repository. It's a good way to keep track of your progress before committing.

#### 2.2.4 Restoring staged files
If you accidentally stage a file that you don't want to commit yet, you can use `git restore --staged` to unstage it:
```
git restore --staged <file>
```
Replace `<file>` with the name of the file you want to unstage. This command will remove the file from the staging area without affecting its contents or history.

## 3. Stashing changes
A **stash** in Git is a way to temporarily save uncommitted changes so that you can switch branches or perform other operations without losing your work. It's like creating a snapshot of your current state, which you can later reapply to your working directory. This is useful when you need to work on something else but don't want to commit half-finished code yet.
### 3.1 Saving uncommitted changes with **stash**
#### 3.1.1 Creating a new **stash**
To create a new **stash**, run:
```
git stash save "Stash description"
```
Replace "Stash description" with a brief description of the changes you're saving (optional). Your changes will be temporarily removed from the working directory and can be reapplied later.

#### 3.1.2 Listing all stashes
To list all your saved stashes, run:
```
git stash list
```
This will display a list of stashes with their IDs, descriptions, and commit messages.

### 3.2 Applying previously stashed changes
#### 3.2.1 Applying a specific stash
To apply a specific **stash**, run:
```
git stash apply <stash_id>
```
Replace `<stash_id>` with the ID of the **stash** you want to apply.

#### 3.2.2 Deleting a specific stash
To delete a specific **stash**, run:
```
git stash drop <stash_id>
```
Replace `<stash_id>` with the ID of the **stash** you want to delete.

### 3.3 Popping a **stash**
To apply and delete a specific **stash** in one step, use `git stash pop`:
```
git stash pop <stash_id>
```
Replace `<stash_id>` with the ID of the **stash** you want to pop. This will reapply the changes from the specified **stash** and remove it from the list.

## 4. Pre-commit Hooks
[Pre-commit](https://pre-commit.com/) is a framework for managing and maintaining multi-language pre-commit hooks. It can help you with tasks such as linting, formatting code, checking for security vulnerabilities, and ensuring consistent coding styles in your project.

To run all the configured hooks on all the files in your repository, use:
```
pre-commit run --all-files
```
This command will execute all the pre-commit hooks for each file in your repository, allowing you to catch and fix any issues before committing.

## 5. Conclusion
This guide should provide you with a solid understanding of how to use Git and Gitlab for our workflow. Remember to always commit often, branch when necessary, and use WIP commits, stashing, pre-commit hooks, and checking the status of your changes to avoid conflicts and keep your work organized. Good luck!
